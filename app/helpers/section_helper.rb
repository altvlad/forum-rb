# Helper methods defined here can be accessed in any controller or view in the application

module Forum
  class App
    module SectionHelper
      # def simple_helper_method
      # ...
      # end
    end

    helpers SectionHelper
  end
end
