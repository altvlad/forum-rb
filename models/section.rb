class Section < ActiveRecord::Base
  has_many :topics, dependent: :destroy
end
