class CreateTopics < ActiveRecord::Migration[5.1]
  def self.up
    create_table :topics do |t|
      t.string :name
      t.references :section, foreign_key: true, index: true
      t.integer :message_count
      t.timestamps null: false
    end
  end

  def self.down
    drop_table :topics
  end
end
